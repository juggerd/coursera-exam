#pragma once

#include "date.h"
#include <iostream>
#include <map>
#include <vector>
#include <algorithm>

using namespace std;

template<class T>
void PrintRange(const T &range_begin, const T &range_end) {
    for (auto it = range_begin; it != range_end; ++it) {
        cout << *it << " ";
    }
    cout << endl;
}

class Database {
public:
    void Add(const Date &date, const string &event);

    void Print(ostream &stream) const;

    template<class Func>
    int RemoveIf(Func func) {

        size_t q = 0;
        for (auto it = storage.begin(); it != storage.end();) {
            auto its = stable_partition(
                    it->second.begin(),
                    it->second.end(),
                    [func, it](const string &s) {
                        return func(it->first, s);
                    });

            if (its != it->second.begin()) {
                q += its - it->second.begin();
                it->second.erase(it->second.begin(), its);
                if (it->second.empty()) {
                    storage.erase(it++);
                }
            } else {
                it++;
            }
        }

        return q;
    }

    template<class Func>
    vector<pair<Date, string>> FindIf(Func func) const {
        vector<pair<Date, string>> result;

        for (auto it = storage.begin(); it != storage.end(); ++it) {
            for (const string &s: it->second) {
                if (func(it->first, s)) {
                    result.push_back(make_pair(it->first, s));
                }
            }
        }

        return result;
    }

    pair<Date, string> Last(const Date &date) const;

private:
    map<Date, vector<string>> storage;
};

ostream &operator<<(ostream &stream, const pair<Date, string> &p);