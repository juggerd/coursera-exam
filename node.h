#pragma once

#include "date.h"
#include <memory>

using namespace std;

enum class Comparison {
    Equal,
    Greater,
    GreaterOrEqual,
    Less,
    LessOrEqual,
    NotEqual
};

enum class LogicalOperation {
    And,
    Or,
};

class Node {
public:
    virtual bool Evaluate(const Date &date, const string &event) = 0;
};

class EmptyNode : public Node {
public:
    bool Evaluate(const Date &date, const string &event) override;
};

class DateComparisonNode : public Node {
public:
    DateComparisonNode(Comparison cmp, Date date)
            : _cmp(cmp), _date(date) {}

    bool Evaluate(const Date &date, const string &event) override;

private:
    const Comparison _cmp;
    const Date _date;
};

class EventComparisonNode : public Node {
public:
    EventComparisonNode(Comparison cmp, string event)
            : _cmp(cmp), _event(event) {}

    bool Evaluate(const Date &date, const string &event) override;

private:
    const Comparison _cmp;
    const string _event;
};

class LogicalOperationNode : public Node {
public:
    LogicalOperationNode(LogicalOperation op,
                         shared_ptr<Node> lhs,
                         shared_ptr<Node> rhs)
            : _op(op), _lhs(lhs), _rhs(rhs) {}

    bool Evaluate(const Date &date, const string &event) override;

private:
    LogicalOperation _op;
    shared_ptr<Node> _lhs, _rhs;
};