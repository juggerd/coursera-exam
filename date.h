#pragma once

#include <iostream>

using namespace std;

class Date {
public:
    Date(const int &year, const int &month, const int &day);

    int GetYear() const;

    int GetMonth() const;

    int GetDay() const;

private:
    int _year, _month, _day;
};

Date ParseDate(istream &is);

bool operator==(const Date &lhs, const Date &rhs);

bool operator<(const Date &lhs, const Date &rhs);

bool operator<=(const Date &lhs, const Date &rhs);

bool operator>(const Date &lhs, const Date &rhs);

bool operator>=(const Date &lhs, const Date &rhs);

bool operator!=(const Date &lhs, const Date &rhs);

ostream &operator<<(ostream &stream, const Date &date);
