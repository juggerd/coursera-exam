#include <string>
#include "node.h"
#include "date.h"

using namespace std;

bool EmptyNode::Evaluate(const Date &date, const string &event) {
    return true;
}

bool DateComparisonNode::Evaluate(const Date &date, const string &event) {
    switch (_cmp) {
        case Comparison::Equal:
            return date == _date;
        case Comparison::Greater:
            return date > _date;
        case Comparison::GreaterOrEqual:
            return date >= _date;
        case Comparison::Less:
            return date < _date;
        case Comparison::LessOrEqual:
            return date <= _date;
        case Comparison::NotEqual:
            return date != _date;
        default:
            return false;
    }
    return false;
}

bool EventComparisonNode::Evaluate(const Date &date, const string &event) {
    switch (_cmp) {
        case Comparison::Equal:
            return event == _event;
        case Comparison::Greater:
            return event > _event;
        case Comparison::GreaterOrEqual:
            return event > _event;
        case Comparison::Less:
            return event < _event;
        case Comparison::LessOrEqual:
            return event <= _event;
        case Comparison::NotEqual:
            return event != _event;
        default:
            return false;
    }
    return false;
}

bool LogicalOperationNode::Evaluate(const Date &date, const string &event) {
    return _op == LogicalOperation::And
           ? _lhs->Evaluate(date, event) && _rhs->Evaluate(date, event)
           : _lhs->Evaluate(date, event) || _rhs->Evaluate(date, event);
}