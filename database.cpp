#include "database.h"

#include <iostream>
#include <algorithm>

using namespace std;

void Database::Add(const Date &date, const string &event) {
    if (!storage.count(date)) {
        storage[date].push_back(event);
        return;
    }
    vector<string> v = storage.at(date);
    auto it = find(v.begin(), v.end(), event);
    if (it == v.end()) {
        storage[date].push_back(event);
    }
}

void Database::Print(ostream &stream) const {
    for (const auto &item : storage) {
        for (const string &event : item.second) {
            stream << item.first << " " << event << endl;
        }
    }
}

pair<Date, string> Database::Last(const Date &date) const {
    if (storage.count(date)) {
        if (storage.at(date).empty()) {
            throw invalid_argument("");
        }
        return make_pair(date, storage.at(date).back());
    }
    auto it = storage.upper_bound(date);
    if (it == storage.begin()) {
        throw invalid_argument("");
    }
    it--;
    if (it->second.empty()) {
        throw invalid_argument("");
    }
    return make_pair(it->first, it->second.back());
}

ostream &operator<<(ostream &stream, const pair<Date, string> &p) {
    stream << p.first << " " << p.second << endl;
    return stream;
}